// App.js
import React from 'react';
import HomeScreen from './source/page/homescreen';

const App = () => {
  return <HomeScreen />;
};

export default App;
