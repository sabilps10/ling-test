// src/index.tsx

import React from 'react';
import {AppRegistry} from 'react-native';
import {Provider} from 'react-redux';
import App from './App';
import configureStore from './source/store/configurestore';
import {name as appName} from './app.json';

const Main = () => (
  <Provider store={configureStore}>
    <App />
  </Provider>
);

AppRegistry.registerComponent(appName, () => Main);
