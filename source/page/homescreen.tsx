import React, {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {
  View,
  Text,
  TextInput,
  Button,
  Switch,
  FlatList,
  StyleSheet,
} from 'react-native';
import {RootState, AppDispatch} from '../store/configurestore';
import {
  searchUser,
  setSortByName,
  setShowLowestRanked,
} from '../redux/reducer/index';

const SearchUserComponent: React.FC = () => {
  const [username, setUsername] = useState('');
  const dispatch: AppDispatch = useDispatch();
  const {searchResult, errorMessage, sortByName, showLowestRanked} =
    useSelector((state: RootState) => state.user);

  const handleSearch = () => {
    dispatch(searchUser(username));
  };

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        value={username}
        onChangeText={setUsername}
        placeholder="Search by user name"
      />
      <Button title="Search" onPress={handleSearch} />
      {errorMessage && <Text style={styles.error}>{errorMessage}</Text>}
      <View style={styles.switchContainer}>
        <View style={styles.switch}>
          <Text>Sort by Name:</Text>
          <Switch
            value={sortByName}
            onValueChange={value => dispatch(setSortByName(value))}
          />
        </View>
        <View style={styles.switch}>
          <Text>Show Lowest Ranked:</Text>
          <Switch
            value={showLowestRanked}
            onValueChange={value => dispatch(setShowLowestRanked(value))}
          />
        </View>
      </View>
      <FlatList
        data={searchResult}
        keyExtractor={item => item.uid}
        renderItem={({item, index}) => (
          <View style={styles.item}>
            <Text>
              {index + 1}. {item.name} - {item.bananas} bananas
            </Text>
          </View>
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  input: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 10,
    paddingHorizontal: 10,
  },
  error: {
    color: 'red',
    marginBottom: 10,
  },
  switchContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  switch: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  item: {
    padding: 10,
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
  },
});

export default SearchUserComponent;
