import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import Data from '../../data/leaderboard.json';

interface User {
  bananas: number;
  lastDayPlayed: string;
  longestStreak: number;
  name: string;
  stars: number;
  subscribed: boolean;
  uid: string;
}

interface UserState {
  users: User[];
  searchResult: User[];
  errorMessage: string | null;
  sortByName: boolean;
  showLowestRanked: boolean;
}

const usersArray: User[] = Object.values(Data);

const initialState: UserState = {
  users: usersArray,
  searchResult: [],
  errorMessage: null,
  sortByName: false,
  showLowestRanked: false,
};

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    searchUser: (_state, _action: PayloadAction<string>) => {},
    setSearchResult: (state, action: PayloadAction<User[]>) => {
      state.searchResult = action.payload;
      state.errorMessage = null;
    },
    setErrorMessage: (state, action: PayloadAction<string>) => {
      state.errorMessage = action.payload;
    },
    setSortByName: (state, action: PayloadAction<boolean>) => {
      state.sortByName = action.payload;
    },
    setShowLowestRanked: (state, action: PayloadAction<boolean>) => {
      state.showLowestRanked = action.payload;
    },
  },
});

export const {
  searchUser,
  setSearchResult,
  setErrorMessage,
  setSortByName,
  setShowLowestRanked,
} = userSlice.actions;
export default userSlice.reducer;
