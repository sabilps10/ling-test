import {put, takeLatest, select} from 'redux-saga/effects';
import {PayloadAction} from '@reduxjs/toolkit';
import {searchUser, setSearchResult, setErrorMessage} from '../reducer/index';
import {RootState} from '../../store/configurestore';

interface User {
  bananas: number;
  lastDayPlayed: string;
  longestStreak: number;
  name: string;
  stars: number;
  subscribed: boolean;
  uid: string;
}

function* handleSearchUser(action: PayloadAction<string>) {
  const username: string = action.payload;
  const users: User[] = yield select((state: RootState) => state.user.users);

  // Fuzzy search
  const searchResults = users.filter(user =>
    user.name.toLowerCase().includes(username.toLowerCase()),
  );

  if (searchResults.length === 0) {
    yield put(
      setErrorMessage(
        'This user name does not exist! Please specify an existing user name!',
      ),
    );
    return;
  }

  // Sort the users by bananas in descending order
  const sortedUsers = [...users].sort((a, b) => b.bananas - a.bananas);

  // Top 10 users
  let topUsers = sortedUsers.slice(0, 10);

  // Check if the searched user is in the top 10
  const searchedUser = users.find(
    user => user.name.toLowerCase() === username.toLowerCase(),
  );
  if (searchedUser) {
    const userIndex = sortedUsers.indexOf(searchedUser);
    if (userIndex >= 10) {
      topUsers[9] = searchedUser;
    }
  }

  // Optionally sort by name
  const sortByName: boolean = yield select(
    (state: RootState) => state.user.sortByName,
  );
  if (sortByName) {
    topUsers.sort((a, b) => a.name.localeCompare(b.name));
  }

  // Optionally show lowest ranked users
  const showLowestRanked: boolean = yield select(
    (state: RootState) => state.user.showLowestRanked,
  );
  if (showLowestRanked) {
    topUsers = [...users]
      .sort((a, b) => a.bananas - b.bananas || a.name.localeCompare(b.name))
      .slice(0, 10);
  }

  yield put(setSearchResult(topUsers));
}

export function* userSaga() {
  yield takeLatest(searchUser.type, handleSearchUser);
}
