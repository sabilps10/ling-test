import {configureStore} from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';
import userReducer from '../redux/reducer/index';
import {userSaga} from '../redux/saga/index';

const sagaMiddleware = createSagaMiddleware();

const store = configureStore({
  reducer: {
    user: userReducer,
  },
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware().concat(sagaMiddleware),
});

sagaMiddleware.run(userSaga);

export default store;
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
